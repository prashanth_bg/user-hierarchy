import pytest
from ..lib import userhierarchy


class TestClass(object):

    @classmethod
    def get_roles(self):
        roles = [
            {
                "Id": 1,
                "Name": "System Administrator",
                "Parent": 0
            },
            {
                "Id": 2,
                "Name": "Location Manager",
                "Parent": 1,
            },
            {
                "Id": 3,
                "Name": "Supervisor",
                "Parent": 2,
            },
            {
                "Id": 4,
                "Name": "Employee",
                "Parent": 3,
            },
            {
                "Id": 5,
                "Name": "Trainer",
                "Parent": 3,
            }
        ]
        return roles

    def get_users(self):
        users = [
            {
                "Id": 1,
                "Name": "Adam Admin",
                "Role": 1
            },
            {
                "Id": 2,
                "Name": "Emily Employee",
                "Role": 4
            },
            {
                "Id": 3,
                "Name": "Sam Supervisor",
                "Role": 3
            },
            {
                "Id": 4,
                "Name": "Mary Manager",
                "Role": 2
            },
            {

                "Id": 5,
                "Name": "Steve Trainer",
                "Role": 5
            }
        ]
        return users

    def test_userhierarchy_methods(self, capsys):
        users = self.get_users()
        roles = self.get_roles()
        uh = userhierarchy.UserHierarchy(roles, users)
        assert uh.get_all_role_ids() == [1, 2, 3, 4, 5]
        print("\nGetting all role ids .. ok")
        assert uh.get_graph_id_to_role_id_map(
        ) == {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
        print("Map role ids to internal sequence ids .. ok")
        assert uh.get_role_id_to_graph_id_map(
        ) == {1: 0, 2: 1, 3: 2, 4: 3, 5: 4}
        print("Map back internal sequence ids to actual role id .. ok")
        assert uh.get_role_associations() == [(1, 0), (2, 1), (3, 2), (4, 2)]
        print("Get role associations mapped to internal ids .. ok")
        assert uh.get_graph_representation() == {1: [0], 2: [
            1], 3: [2], 4: [2]}
        print(
            "Get graph representation to be used as input to topological dag sorting .. ok")
        assert uh.get_sorted_dependencies() == [5, 4, 3, 2, 1]
        print("Get sorted order of dag dependencies by calling topological sort .. ok")
        assert uh.get_role_id_of_user(2) == 4
        print("Return role id of a user given it's id .. ok")
        assert uh.get_associated_role_ids() == [1, 2, 3]
        print("Return all role ids which are associated with any other role id, ignore 0 .. ok")
        assert uh.get_leaf_nodes() == [4, 5]
        print("Get leaf nodes - node which doesn't have any children .. ok")
        assert uh.get_users_having_roles([2, 3, 0]) == [{'Id': 3, 'Name': 'Sam Supervisor', 'Role': 3},
                                                        {'Id': 4, 'Name': 'Mary Manager', 'Role': 2}]
        print("Given a list of roles, return users having those roles .. ok")

    def test_integration(self, capsys):
        users = self.get_users()
        roles = self.get_roles()
        uh = userhierarchy.UserHierarchy(roles, users)
        assert uh.get_sub_ordinates(2) == []
        print("\nGet subordinates for user id 2 (none) .. ok")
        assert uh.get_sub_ordinates(
            3) == [{'Id': 2, 'Name': 'Emily Employee', 'Role': 4}, {'Id': 5, 'Name': 'Steve Trainer', 'Role': 5}]
        print("Get subordinates for user id 3 .. ok")
        assert uh.get_sub_ordinates(100) == []
        print("\nGet subordinates for non existing user id 100 (none) .. ok")
