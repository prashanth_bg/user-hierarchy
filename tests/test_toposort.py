import pytest
from ..lib import toposort


class TestClass(object):

    @classmethod
    def get_test_graph(self):
        graph = {
            0: ([1]),
            5: ([1]),
            1: ([2, 3]),
            2: ([4])
        }
        return graph

    def test_toposort(self, capsys):
        graph = self.get_test_graph()
        ts = toposort.TopoSort(graph, 6)
        assert ts.topo_sort() == [5, 0, 1, 3, 2, 4]
        print("Topological sorting for a simple graph .. ok")
