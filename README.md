# User hierarchy traversal based on roles.

The solution utilizes the idea of graph data structures. The role hierarchy has parent child structure and graphs represent these kind of relationships the best.  

Below is a short description of how the solution works:

* The input role data-structure is converted into a graph with parent vertices as keys and associated roles as value array/list.  
* This graph is then sorted by the order of dependencies using Kahn's algorithm (Topological sorting). The highest role is at the right most.  
* For a given user, the associated role is looked up in the topologically sorted array and all roles to the left(dependent roles) are returned.  
* If the user has a role which are leaf nodes (no subordinates), an empty list/array is returned.  
* If the user is not amongst the list of users, empty list is returned.  


The implementation has three files.  

lib contains the following classes:   
1. toposort.py - Implements topological sorting  
2. userhierarchy.py - Implements api to set users and roles and to retrieve the subordinate roles.  
root directory contains:  
3. client.py which is the driver program to call the above classes.  


The solution is implemented in Python 3. The Api is as below  


```
roles = [...]
users = [...]
#init instance and pass roles and users to the constructor (Python does not generally use getters an setters)
uh = uh.UserHierarchy(roles, users)
#After this, the below returns the subordinate roles
print(uh.get_sub_ordinates(3))

```

There is a test suite using pytest to cover most of the methods along with some negative test cases. Please see below to run those.

### Prerequisites

Install Python 3
Install pip3

Install pytest test framework with the below command

```
pip3 install -r requirements.txt
```

### Running the program

To run the program - From the main directory, run

```
python client.py
```

To modify the inputs, edit the client file and supply the user_id

## Running the tests

To run the test suite

```
#From the main directory
pytest -vs
```
