"""
    Simple implementation of Kahn's Topological sorting algorithm.
    Assumption is the graph is directed, acyclic and has unique ids.
    parameters to instantiate the class: 
    graph: {1:[2,3], 3:[4], 5:[]} 
    number: number of nodes 
"""


class TopoSort(object):

    def __init__(self, graph, n):
        self._graph = graph
        self._n = n

    def topo_sort(self):
        indegree = [0 for i in range(self._n)]
        zero_indegree = []
        topo_ordered = []
        for node in self._graph:
            for descendant in self._graph[node]:
                indegree[descendant] = indegree[descendant] + 1

        zero_indegree = [i for i in range(len(indegree)) if indegree[i] == 0]

        while (len(zero_indegree) != 0):
            topo_ordered.append(zero_indegree.pop())
            if topo_ordered[-1] in self._graph:
                for neighbor in self._graph[topo_ordered[-1]]:
                    indegree[neighbor] -= 1
                    if indegree[neighbor] == 0:
                        zero_indegree.append(neighbor)

        return topo_ordered
