from .import toposort as tsort


"""
    API implementation
"""


class UserHierarchy(object):
    def __init__(self, roles, users):
        self._roles = roles
        self._users = users

    def get_sorted_dag_deps(self, graph, size):
        """
            get topologically sorted nodes in order of dependencies 
        """
        ts = tsort.TopoSort(graph, size)
        dependency_graph = ts.topo_sort()
        return dependency_graph

    def get_all_role_ids(self):
        """
            get all existing role ids
        """
        return [(item['Id']) for item in self._roles]

    def get_graph_id_to_role_id_map(self):
        """
            maps actual role id to internal id sequence (role id need not be num)
        """
        role_id_list = self.get_all_role_ids()
        return dict(zip(range(0, len(self._roles)), role_id_list))

    def get_role_id_to_graph_id_map(self):
        """
            maps internal id's to actual role.
        """
        mappings = self.get_graph_id_to_role_id_map()
        return {v: k for k, v in mappings.items()}

    def get_role_associations(self):
        """
            return role id to parent id associations translated to internal sequence ids.
            Ignore if parent id is 0
        """
        rid_gid_map = self.get_role_id_to_graph_id_map()
        role_associations = [(rid_gid_map[item['Id']], rid_gid_map[item['Parent']])
                             for item in self._roles if item['Parent'] != 0]
        return role_associations

    def get_graph_representation(self):
        """
            reshape graph representation to fit TopoSort's topo_sort method.
            Dict with keys as node and values as list of associated nodes.
        """
        associations = self.get_role_associations()
        graph = dict([([t[0], [t[1]]]) for t in associations])
        return graph

    def get_sorted_dependencies(self):
        """
            get graph representation, get sorted nodes, remap result to actual 
            keys as per input data
        """
        g = self.get_graph_representation()
        dg = self.get_sorted_dag_deps(g, len(self._roles))
        id_map = self.get_graph_id_to_role_id_map()
        remapped_graph = [id_map[node] for node in dg]
        return remapped_graph

    # user
    def get_role_id_of_user(self, id):
        """
            return role id of a given user
        """
        role_id = [user['Role']
                   for user in self._users if user['Id'] == id]
        try:
            return role_id[0]
        except:
            return None

    def get_associated_role_ids(self):
        """
            returns only those role ids which are linked to other role ids
            removes duplicates, ignores role id 0
        """
        linked_role_ids = list(set([item['Parent']
                                    for item in self._roles if item['Parent'] != 0]))
        return linked_role_ids

    def get_leaf_nodes(self):
        """
            return roles which doesn't have any associations/children
        """
        id_list = self.get_all_role_ids()
        associated_role_id = self.get_associated_role_ids()
        nodes = list(set(id_list) - set(associated_role_id))
        return nodes

    def get_users_having_roles(self, role_list):
        """
            return subset of users having any of the roles in input list
        """
        users = [user for user in self._users if user['Role'] in role_list]
        return users

    def get_sub_ordinates(self, user_id):
        """
            get role of input user, get sorted dpendencies of roles,
            find leaf nodes to exclude, get users with resulting roles. 
        """
        user_role_id = self.get_role_id_of_user(user_id)
        leaf_nodes = self.get_leaf_nodes()

        try:
            # list of roles in sorted order of their dependencies
            sorted_deps = self.get_sorted_dependencies()
            pos = sorted_deps.index(user_role_id)
        except IndexError:
            return []
        except ValueError:
            return []
        except:
            return []

        # leaf has no chlid
        if(user_role_id in leaf_nodes):
            return []

        # all roles to the left of pos are subordinate roles
        subordinate_roles = sorted_deps[:pos]

        subordinates = self.get_users_having_roles(subordinate_roles)
        return subordinates
